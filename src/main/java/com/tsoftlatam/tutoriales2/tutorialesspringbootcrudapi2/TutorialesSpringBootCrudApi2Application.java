package com.tsoftlatam.tutoriales2.tutorialesspringbootcrudapi2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorialesSpringBootCrudApi2Application {

	public static void main(String[] args) {
		SpringApplication.run(TutorialesSpringBootCrudApi2Application.class, args);
	}

}
