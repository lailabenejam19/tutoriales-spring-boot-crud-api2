package com.tsoftlatam.tutoriales2.tutorialesspringbootcrudapi2.course;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class CoursesHardcodedService {

//Lista de cursos hardcodeados
private static List<Course> courses = new ArrayList<>();
 
private static long idCounter = 0;
 
 //devolución de la Lista 
 public List<Course> findAll() {
	 return courses;
 }
 
 
 public Course findById(long id) {
     for (Course course: courses) {
       if (course.getId() == id) {
         return course;
       }
     }
     return null;
 }
  
 public Course deleteById(long id) {
	 Course course = findById(id);
	 if (course == null)
	 return null;
	 if (courses.remove(course)) {
	 return course;
	 }
		 return null;
	 }
 
 public Course save(Course course) {
	 if (course.getId() == -1 || course.getId() == 0) {
	 course.setId(++idCounter);
	 courses.add(course);
	 } else {
	 deleteById(course.getId());
	 courses.add(course);
	 }
	 return course;
	}

}
