package com.tsoftlatam.tutoriales2.tutorialesspringbootcrudapi2.course;

//import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.tsoftlatam.tutoriales2.tutorialesspringbootcrudapi2.DTO.CourseDTO;
import com.tsoftlatam.tutoriales2.tutorialesspringbootcrudapi2.instructor.Instructor;
import com.tsoftlatam.tutoriales2.tutorialesspringbootcrudapi2.instructor.InstructorRepository;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })

//Recuperar lista de cursos 
@RestController
//s@RequestMapping("/instructors/")
public class CourseResource {
	
// @Autowired
//private CoursesHardcodedService courseManagementService;
	
	@Autowired
	private CourseRepository courseRepository;
	@Autowired
	private InstructorRepository intructorsRepository;
	
 
	
    @GetMapping("/courses")
    public ResponseEntity<List<CourseDTO>> getAllCourses(){
        try {
                       
            List<Course> courses = new ArrayList<Course>();
            List<CourseDTO> courseDTO = new ArrayList<CourseDTO>();
            Instructor intructorById = new Instructor();
            
            courseRepository.findAll().forEach(courses::add);
           
            for(Course curso : courses) {
               
                Optional<Instructor> InstructorTemp = null;
               
                
                if(curso.getUsername() != null || curso.getUsername() != 0 || curso.getUsername() != '0') {
                    InstructorTemp =  intructorsRepository.findById(curso.getUsername());
                }
               
               
                if(InstructorTemp.isPresent()) {
                    intructorById = InstructorTemp.get();
                    courseDTO.add(new CourseDTO(
                            curso.getId(),
                            curso.getDescription(),
                            intructorById.getUsername(),
                            intructorById.getName() + " " + intructorById.getLastname(),
                            intructorById.getId()));
                    		
                }
            }
                                   
            if(courseDTO.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(courseDTO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	/*
	@GetMapping("/courses")
	public ResponseEntity<List<Course>> getAllCourses(){
		try {
			List<Course> courses = new ArrayList<Course>();
			courseRepository.findAll().forEach(courses::add);
			if(courses.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(courses, HttpStatus.OK);
			
		}catch(Exception e) 
		{
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
 */
 @GetMapping("/courses/{id}")
 public  ResponseEntity<Course> getCourse(@PathVariable long id) {
  Optional<Course> courseData = courseRepository.findById(id);
  
  if(courseData.isPresent()) {
	  return new ResponseEntity<>(courseData.get(),HttpStatus.OK);
  }
  else {
	  return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }
 }
 
 
 
 @GetMapping("/instructors/{username}/courses")
 public ResponseEntity<List<Course>> getAllByUsername(@PathVariable String username){
	 try {
		 List<Instructor> instructor = new ArrayList<Instructor>();
		 intructorsRepository.findByUsername(username).forEach(instructor::add);
		 if( instructor.isEmpty()) {
			 return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		 }
		 System.out.println(instructor.get(0).getId());
		 List<Course> courses = new ArrayList<Course>();
		 courseRepository.findByUsername(instructor.get(0).getId()).forEach(courses::add);
		 if(courses.isEmpty()) {
			 return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		 }
		 
		 return new ResponseEntity<>(courses,HttpStatus.OK);
	 }catch(Exception e) {
		 return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
	 }
 }

 @PutMapping("/courses/{id}")
 public ResponseEntity<Course> updateCourse(@PathVariable long id,
 @RequestBody Course course) { 
	 Optional<Course> courseData = courseRepository.findById(id);
	 Course _courseData = courseData.get();
	 _courseData.setUsername(course.getUsername());
	 _courseData.setDescription(course.getDescription());
	 
 return new ResponseEntity<>(courseRepository.save(_courseData), HttpStatus.OK);
 }

 
 @PostMapping("/courses")
 public ResponseEntity<Course> createCourse(@RequestBody Course course) {
	 try {
		 Course _course = courseRepository.save(new Course(course.getUsername(), course.getDescription()));
		 return new ResponseEntity<>(_course,HttpStatus.CREATED);
		 
	 }catch(Exception e) {
		 return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
	 }
 }

 @DeleteMapping("/courses/{id}")
 public ResponseEntity<Void> deleteCourse(@PathVariable long id) {

   try {
       courseRepository.deleteById(id);
       return new ResponseEntity<>(HttpStatus.NO_CONTENT);
   } catch(Exception e){
       return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
   }
 }
 
}