package com.tsoftlatam.tutoriales2.tutorialesspringbootcrudapi2.course;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository <Course, Long> {

	List<Course> findByUsername(Long username);
}
