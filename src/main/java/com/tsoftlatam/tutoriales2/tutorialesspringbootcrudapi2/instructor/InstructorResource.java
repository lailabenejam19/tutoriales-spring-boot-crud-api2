package com.tsoftlatam.tutoriales2.tutorialesspringbootcrudapi2.instructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tsoftlatam.tutoriales2.tutorialesspringbootcrudapi2.course.Course;
import com.tsoftlatam.tutoriales2.tutorialesspringbootcrudapi2.course.CourseRepository;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })

@RestController
public class InstructorResource {
 
@Autowired
private InstructorRepository instructorRepository;	
@Autowired
private CourseRepository courseRepository;
	
 @GetMapping("/instructors")
 public ResponseEntity<List<Instructor>> getAllInstructor(){
		try {
			List<Instructor> instructor = new ArrayList<Instructor>();
			instructorRepository.findAll().forEach(instructor::add);
			if(instructor.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(instructor, HttpStatus.OK);
			
		}catch(Exception e) 
		{
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
 }

 @GetMapping("/instructors/{id}")
 public  ResponseEntity<Instructor> getInstructor(@PathVariable long id) {
  Optional<Instructor> instructorData = instructorRepository.findById(id);
  
  if(instructorData.isPresent()) {
	  return new ResponseEntity<>(instructorData.get(),HttpStatus.OK);
  }
  else {
	  return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }
 }
 
 @PutMapping("/instructors/{id}")
 public ResponseEntity<Instructor> updateInstructor(@PathVariable long id,
 @RequestBody Instructor instructor) {
 	 
	 Optional<Instructor> instructorData = instructorRepository.findById(id);
	 Instructor _instructorData = instructorData.get();
	 _instructorData.setUsername(instructor.getUsername());
	 _instructorData.setName(instructor.getName());
	 _instructorData.setLastName(instructor.getLastname());
	 
 return new ResponseEntity<>(instructorRepository.save(_instructorData), HttpStatus.OK);

 }
 
 
 @PostMapping("/instructors")
 public ResponseEntity<Instructor> createInstructor(@RequestBody Instructor instructor) {

	 try {
		 Instructor _instructor = instructorRepository.save(new Instructor(instructor.getUsername(), instructor.getName(), instructor.getLastname()));
		 return new ResponseEntity<>(_instructor,HttpStatus.CREATED);
		 
	 }catch(Exception e) {
		 return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
	 }
 }
 
 @DeleteMapping("/instructors/{id}")
 public ResponseEntity<Void> deleteInstructor(@PathVariable long id) {
   try {
	   List<Course> courseToDelete = new ArrayList<Course>();
		 courseRepository.findByUsername(id).forEach(courseToDelete::add);
		 for (Course course : courseToDelete) {
			 courseRepository.deleteById(course.getId());
		 }
	   instructorRepository.deleteById(id);
       return new ResponseEntity<>(HttpStatus.NO_CONTENT);
   } catch(Exception e){
       return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
   }
 }
 
}
