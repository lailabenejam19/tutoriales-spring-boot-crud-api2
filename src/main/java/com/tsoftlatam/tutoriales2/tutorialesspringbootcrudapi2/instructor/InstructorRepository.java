package com.tsoftlatam.tutoriales2.tutorialesspringbootcrudapi2.instructor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InstructorRepository extends JpaRepository <Instructor, Long>{
	
	List<Instructor>findByUsername(String username);
}
