package com.tsoftlatam.tutoriales2.tutorialesspringbootcrudapi2.instructor;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class InstructorHardcodedService {

private static List<Instructor> instructors = new ArrayList<>();
private static long idCounter = 0;

public List<Instructor> findAll(){
	return instructors;
}

public Instructor findById(long id) {
    for (Instructor instructor: instructors) {
      if (instructor.getId() == id) {
        return instructor;
      }
    }
    return null;
}

public Instructor deleteById(long id) {
	 Instructor instructor = findById(id);
	 if (instructor == null)
	 return null;
	 if (instructors.remove(instructor)) {
	 return instructor;
	 }
		 return null;
	 }

public Instructor save(Instructor instructor) {
	 if (instructor.getId() == -1 || instructor.getId() == 0) {
		 instructor.setId(++idCounter);
		 instructors.add(instructor);
	 } else {
	 deleteById(instructor.getId());
	 instructors.add(instructor);
	 }
	 return instructor;
	}

}