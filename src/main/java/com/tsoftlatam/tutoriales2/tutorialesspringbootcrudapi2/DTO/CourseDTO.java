package com.tsoftlatam.tutoriales2.tutorialesspringbootcrudapi2.DTO;

import java.io.Serializable;

public class CourseDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long  id;
	private String description;
	private String username;
	private String instructorFullName;
	private Long idInstructor;
	
	
	public CourseDTO(Long id, String description, String username, String instructorFullName,Long idInstructor) {
		super();
		this.id=id;
		this.description=description;
		this.username=username;
		this.instructorFullName=instructorFullName;
		this.idInstructor=idInstructor;		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getInstructorFullName() {
		return instructorFullName;
	}
	public void setInstructorFullName(String idInstructor) {
		this.instructorFullName = idInstructor;
	}
	public Long getIdInstructor() {
		return idInstructor;
	}
	public void setIdInstructor(Long idInstructor) {
		this.idInstructor = idInstructor;
	}
	
}
